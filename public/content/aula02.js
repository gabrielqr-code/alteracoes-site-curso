const aula02_content = [
    '<a href="doc/Aula02.pdf" target="_blank">Slide</a>' + 
    '<h2> O que é Software Livre? </h2>' +
    '<p> Software Livre pode ser entendido como um software que respeita as seguintes regras:</p>'+
        '<li> Vocẽ deve poder executar o programa como você desejar, para qualquer propósito</li>' + 
        '<li> Você deve poder estudar como o programa funciona, e poder adaptá-lo às suas necessidades</li>' + 
        '<li> Você deve ser capaz de redistribuir cópias</li>' +
        '<li> Você deve ser capaz de redistribuir cópias de suas versões modificadas</li>' +
      '<p>  Essas são chamadas <b> As Quatro Liberdades Essenciais. </b> </p>' +
      '<p> Para adequadamente entender software livre, deve-se colocar em questão a noção de propriedade intelectual. ' +
      'Propriedade intelectual é uma metáfora, pois afinal, ao contrário da propriedade real, não há escassez. </p>' +
      '<p>Uma ideia não é um objeto. Ao transmitir o que é pensado, a ideia não some da mente que compartilhou. Sendo assim, de onde ' +
      'surgiu essa limitação de propriedade intelectual? No passado, essas leis surgiram como um compromisso entre público e privado - de início o lucro existiria, e posteriormente o conhecimento seria de todos.'+
      '<p> Porém essa era uma limitação da Europa do século XVII, tratava de patentes de livro e tinha pouco impacto por que nem todos tinham uma máquina de Gutemberg. Hoje em dia, porém, há muitos computadores. O impacto é imenso.'+
      '<p> Não é estranho que conforme a tecnologia avança, as patentes tenham durado mais e não menos?</p>'+
      '<h2> Motivos para usar Software Livre </h2>' +
      '<p> Há diversas vias para defender o uso do software livre. Algumas delas:'+
        '<li><b> Ética:</b> conhecimento deve ser público! </li>'+
        '<li><b> Impacto Social:</b> conhecimento importante demais para ficar nas mãos de algum grupo</li>'+
        '<li><b> Metodologia:</b> vantagens metodológicas, técnicas e de mercado do Software Livre</li>'+
      '<p>Existem outros motivos, esses são só alguns deles. Por exemplo, no assunto das vantagens metodológicas, podemos pensar que múltiplos olhares sob o programa tendem a produzir algo mais interessante, mais rápido.</p>' +
      '<h2> Código Fonte </h2>' +
      '<p> O código fonte é o código utilizado para produzir o programa. O acesso ao ' +
      'código fonte é essencial para que o software possa ser considerado livre, afinal, como respeitar a regra do estudo sem ter acesso a esse código? </p>' +
      '<h2>Riscos do Software Livre</h2>' +
      '<p>Ao contrário do que muitos pensam, os riscos financeiros do software livre são muito baixos, e muitos softwares importantes são livres. </p>' +
      '<p>Por exemplo, apenas 20% do lucro de empresas de software provém de licenças. Todo o resto provém de serviços. 85% dos trabalhadores de TI' +
      'trabalham em empresas de software como serviço, e não software como produto fechado.</p>'
  ];
