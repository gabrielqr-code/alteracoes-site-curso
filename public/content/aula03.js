const aula03_content = [
        '<a href="doc/Aula03.pdf" target="_blank">Slide</a>' +
        '<h2>Afinal, o que é esse tal de Linux?</h2>' +
        '<p>É um sistema operacional assim como os outros, onde você pode rodar programas e armazenar arquivos assim como nos demais.</p>'+
        '<p>O que é o "pseudo-terminal", ou apenas terminal, do Linux? O nome pseudo-terminal surgiu de uma época em que o computador em si era o mainframe e a comunicação mainframe-programador, e vice-versa, era feita por meio do terminal, que nada mais era que uma tela e um teclado onde se era colocado um input que era levado até o mainframe e era devolvido um output no mesmo terminal. Com o passar dos tempos o computador passou a fazer as duas funções e hoje não há diferença de nomenclatura.</p>'+
        '<p>Mas basicamente o terminal é o sistema operacional em linha de comando, que pode ser chamado por alt+ctrl+f1,f2,f3,f4,f5,f6(cada um corresponde a um terminal diferente, podendo serem utilizados diferentes usuários) onde há um loop de:</p>'+
        '<ul>'+
                '<li> Pede comando;</li>'+
                '<li> Lê comando;</li>'+
                '<li> Executa comando;</li>'+
        '</ul>'+
        '<p>Quem faz esse serviço no terminal é o shell, existem muitos programas diferentes de shell, porém o mais utilizado na maioria das distribuições de Linux é o Bash(Bourne Again SHell).</p>'+
        '<p>Agora você pode estar se perguntando:"Tá mas como que eu uso no meu dia-a-dia um sistema operacional apenas em linha de comando?"</p>'+
        '<p>Pois não se preocupe para todo, ou quase todo, problema há uma solução, e nesse caso a solução são os AMBIENTES GRÁFICOS!!!</p>'+
        '<h2>Diversos Ambientes Gráficos e O que fazem</h2>'+
        '<p>Assim como as diversas distribuições de Linux em si, existem vários ambientes gráficos, entre eles, alguns são: Gnome, Cynnamon, KDE, XFCE, Mate, etc... </p>'+
        '<p>O que esses ambientes gráficos nos proporcionam:</p>'+
        '<ul>'+
                '<li>Ícones para ativar aplicativos;</li>'+
                '<li>Menus e painéis para organizar os ícones;</li>'+
                '<li>Integração entre os aplicativos;</li>'+
                '<li>Mecanismos de configuração e mudança de aparência;</li>'+
                '<li>Aplicativos que são fáceis de acessar em alguns ambientes gráficos dão mais trabalho em outros.</li>'+
                '<li>MUITA personalização</li>'+
        '</ul>'+
        '<p>Alguns exemplos de ambientes gráficos mostrados em aula e disponíveis nos slides dessa aula:</p>'+
        '<img src="img/aula03_01.png" width=100%>' +
        '<p> </p>'+
        '<img src="img/aula03_02.png" width=100%>' +
        '<p>Nessas duas imagens é possível ter uma ideia de quão diferente os ambientes gráficos podem se parecer.</p>'+
        '<p>Porém não se engane, sempre que você clica em um ícone no ambiente gráfico, o sistema manda executar um comando, e esse comando nada mais é que uma linha de código assim como no console do terminal. </p>'+
        '<p>Mas afinal, como é um comando? O que eles fazem? Tipicamente, não em todos os casos claro. teremos uma estrutura parecida com:</p>'+
        '<p>"comando parâmetro(s) arquivo" </p>'+
        '<p>Dentre esses comandos existem os internos ao Shell, chamados de "builtins" e os externos, que vivem em arquivos:/usr/bin,/usr/sbin,...</p>'+
        '<p>Mas como vou lembrar todos os comandos e todos os parâmetros possíveis para poder executá-los pelo console?</p>'+
        '<p>Pois novamente, não se preocupe, existe um comando, ou melhor dois, que serão seus melhores amigos:"help" e "man".</p>'+
        '<p>O help te mostrará uma lista de comandos builtins ao shell e pode-se adicionar um comando ao help, de modo a ter "help comando " de forma a receber especificamente o que o comando pode ter como parâmetro, o que ele faz e como ele deve se comportar em determinados casos.</p>'+
        '<p>Já o man tem a mesma função do help porém funciona para comandos externos ao shell.</p>'+
        '<p>Nos slides da aula estão alguns comandos de uso geral que aconselhamos muito que vocês testem e mexam um pouco com eles para entender melhor seu funcionamento, lembrando que sempre se pode pesquisar mais comandos na internet.</p>'+
        '<h2>Aliases: o que são e por que quero usá-los?</h2>'+
        '<p>Agora que já entendemos o que são os comandos e o que eles fazem você pode estar pensando "Okay, agora eu já sei usar os comandos em arquivos específicos para manipulá-los. Mas e se eu quiser utilizar um comando específico com um mesmo parâmetro várias vezes em diferentes ocasiões em diferentes arquivos?"</p>'+
        '<p>Primeiro que UAU, eu admiro a capacidade de ter esse exato pensamento neste momento do treinamento. Segundo que, sim há uma solução para isso e a solução são os aliases. Um alias basicamente é um apelido que você acaba dando para um comando para que ele possa ser chamado no futuro com uma mesma escolha determinada de parâmetros.</p>' +
        '<h2>Redirecionamento</h2>'+
        '<p>O Linux trata os arquivos como sequência de bytes. Três fluxos de bytes vem por default; shell interativo acopla a dispositivos físicos:</p>'+
        '<ul>'+
                '<li>Entrada padrão (stdin) = teclado. Porém o fluxo de entrada pode ser redefinida utilizando "<"; </li>'+
                '<li>Saída padrão (stdout) = terminal. Também pode ter o fluxo de saída redefinido utilizando ">"; </li>'+
                '<li>Erro padrão (stdeer) = terminal. Pode-se alterar o fluxo de saída do erro utilizando "2>". </li>'+
        '</ul>'

];
