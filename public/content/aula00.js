const aula00_content = [
    '<a href="doc/Aula00.pdf" target="_blank">Slide</a>' + 
    '<br><br>' +
    '<iframe width="560" height="315" src="https://www.youtube.com/embed/xmgY4Bvyo-8" title="Aula 0 - Introdução" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
    '<br><br>'+
    '<h2>O que é a Rede Linux?</h2>' +
    '<p>É uma rede de computadores Debian que está disponível para alunos do IME. Ela possui 3 salas, todas situadas no Bloco A:</p>' +
    '<ul>' + 
      '<li>125 - Sala da Administração</li>' + 
      '<li>127 - Laboratório Geral</li>' + 
      '<li>258 - Laboratório do BCC</li>' +
    '</ul>' + 
    '<p>Sendo um administrador da Rede, as suas responsabilidades são:</p>' +
    '<ul>' +
      '<li>Atender os usuários, tirando suas dúvidas e resolvendo seus problemas relacionados à rede;</li>' +
      '<li>Realizar atendimento presencial;</li>' +
      '<li>Responder e-mails;</li>' +
      '<li>Fazer a manutenção, atualização e gestão dos equipamentos da Rede.</li>' +
    '</ul>' +
    '<p>São, no mínimo, 20 horas semanais dessas atividades, sendo 4 delas reservadas para o atendimento presencial. As outras 16 horas para a manutenção e gestão da Rede. Porém, caso a Rede esteja em chamas, como às vezes acontece - você vai precisar se dedicar até que o problema seja resolvido, mesmo que no final do semestre.</p>' +
    '<p>A bolsa oferecida é de 500 reais mensais. Seu maior ganho com a Rede Linux será conhecimentos técnicos e sociais (como trabalhar em grupo, gestão de projetos, contato com usuários, etc).</p>'+
    '<h2>Procedimentos</h2>' +
    '<p>Como admin, seu cotidiano girará em torno de procedimentos básicos, sendo eles:</p>' +
    '<ul>' +
      '<li>Criar contas;</li>' +
      '<li>Mudar senha;</li>' +
      '<li>Liberar acesso remoto;</li>' +
      '<li>Instalar pacotes;</li>' +
      '<li>Suspender contas (vejam as regras de uso no site!);</li>' +
      '<li>Arrumar a impressora;</li>' +
      '<li>Limpar máquinas;</li>' +
      '<li>Recrutar novos admins.</li>' +
    '</ul>' +
    '<p>Durante a pandemia, foi elaborado um guia para esses procedimentos comuns e você pode apenas seguir ele quando for necessário.</p>' +
    '<p>Porém, há momentos como agora, que o admin precisa executar tarefas mais complexas e técnicas, como </p>' +
    '<ul>' +
      '<li>Hospedar sites (Gamedev é um exemplo);</li>' +
      '<li>Realizar configurações de rede (NFS, Kerberos, cups, etc);</li>' +
      '<li>Criação e manutenção de scripts.</li>' +
    '</ul>' +
    '<p>Durante este curso, vamos passar brevemente, porém citando uma bibliografia muito valiosa, por quase todos os assuntos técnicos pertinentes a Rede.</p>' +
    '<h2>História da Rede Linux</h2>' +
    '<p>Em 1994 havia, no IME, algumas estações de trabalho com acesso muito limitado, geralmente reservado para pesquisa, e sem acesso para o público em geral. As estações rodavam UNIX e podiam acessar a internet, coisa ainda rara na época. Foi por meio de um projeto de IC que Adriano Rodrigues e Félix de Almeida passaram a estudar essas máquinas, e, com o surgimento e popularidade do Linux, tiveram uma ideia: por insistência deles e de alguns outros alunos, em 1995 foi cedida a sala 125 (a atual administração) junto com quatro microcomputadores, formando um núcleo de apoio para pessoas se aprofundarem e estudarem Linux.</p>' +
    '<p>Esse grupo de apoio recebeu acesso à rede principal do IME e foi batizado de GUL - Grupo de Usuários Linux. Depois de um tempo, em 1997 foi dado acesso a todos os alunos do BCC, o que acabou gerando grande destaque e importância para a rede, fazendo com que mais recursos fossem ganhos, formando assim a Rede Linux.</p>' +
    '<p>O GUL ganhou o prêmio Os Vencedores da revista Info em nome de toda a comunidade Linux do Brasil, recebendo o prêmio em um evento de gala no Hotel Renaissance que contou com a presença de vários representantes importantes de informática do Brasil.</p>' +
    '<p>Em 1999 a Rede Linux recebeu apoio do Programa Pró-Aluno, passando a atender a todas as graduações do IME.</p>' +
    '<h3>Passado Recente</h3>' +
    '<p>A geração anterior da Rede foi formada por alunos do IME e da FFLCH. Tadeu e Zé encontraram a rede não-documentada e desatualizada, e se colocaram no trabalho descomunal de refazer muita coisa e começaram a documentar os componentes da Rede.</p>' +
    '<p>O esforço de documentar a Rede trouxe grandes ganhos, mas ele ainda é bem inicial, temos páginas como:</p>' +
    '<img src="img/virt.png" width=100%>' +
    '<img src="img/aibosta.png" width=100%>' +
    '<h3>Dias de Hoje</h3>' +
    '<p>Durante a pandemia, muitos admins sairam. Uma equipe de 7 virou apenas de 3 pessoas. Os admins restantes não estavam por dentro do que os admins anteriores fizeram, a Rede não foi atualizada durante a pandemia e agora estamos sobrecarregados! Este é um dos momentos em que a Rede está instável, pegando fogo, e precisamos de vocês! Temos muitos desafios pela frente.</p>' +
    '<h2>Objetivo deste Treinamento</h2>' +
    '<p>Como vocês devem ter notado, temos muito trabalho a fazer e estamos buscando pessoas entusiasmadas com redes e autodidatas! É uma grande oportunidade de operar uma rede deste tipo. Vocês terão bastante liberdade para fazer o que quiserem desde que não firam as regras e princípios da rede. Podem mudar o sistema operacional, quotas de impressão, sistema de impressão, etc. Essa é uma das grandes diferenças entre ser monitor, em outras redes Pró-Aluno, e ser admin na Rede Linux - aqui você realmente tem autonomia para gerir a rede como achar melhor, desde que garanta que ela funcione.</p>' +
    '<p>Nosso objetivo com o treinamento, além de difundir o conhecimento, é que os novos admins entrem menos perdidos e estejam cientes do que esperar e o que não esperar. Às vezes a Rede está estável e nosso trabalho é tranquilo, às vezes está tudo pegando fogo, as pessoas estão te pressionando e você não sabe o que fazer - e, na maioria das vezes, não terá ninguém para te ajudar. E não importa se é final do semestre, a responsabilidade da rede é sua. Ser capaz de manter a rede é cansativo, porém gratificante. Ajuda no seu currículo e melhora significativamente seus conhecimentos sobre computação e linux, além de outras soft skills.</p>' +
    '<p>Caso você tenha planos de gerir qualquer outro projeto pessoal no futuro, esta é uma ótima oportunidade de ingressar como responsável em um projeto que já existe e aprender a gerenciar e manter o produto, trabalhar em grupo e lidar com crises - acredite, pode parecer assustador, mas você consegue.</p>' +
    '<p>Também é uma ótima oportunidade para entusiastas de Linux colocarem seus conhecimentos em prática e aumentá-los exponencialmente!</p>' +
    '<h3>"Mas poxa, não sei nada de Linux ou redes, então não é pra mim?"</h3>' +
    '<p>Muitos admins entram sem saber nada e saem: Sabendo nada, sabendo algo ou sabendo muito. O que difere esses três tipos de pessoa é o quão autodidata e proativa ela é. O mais importante é ter interesse e ser inclinado a resolver problemas sobre sua própria responsabilidade.</p>' +
    '<h3>"Não sou autodidata, não manjo e tenho medo de tomar iniciativa e de me responsabilizar, ainda mais com tantos usuários!"</h3>' +
    '<p>Eu também era assim, você pode se desafiar e mudar isso e sair alguém mais preparado para abraçar desafios, mas, se você não quiser, um dos grandes problemas atuais da rede é que pessoas muito empolgadas entram, fazem grandes mudanças positivas, não documentam nada e vão embora, deixando os novos admins sem entender nada do que foi feito e tudo vira lixo quando alguém muito bom joga fora e refaz tudo sem documentar, repetindo o ciclo. No meio tempo, a rede fica uma bosta apenas respirando por aparelhos enquanto as coisas desatualizam. Você é bom escrevendo, trabalhando em equipe e se comunicando? Então documente as coisas! Trabalhe em prol de deixar a  Rede estável, compreensível, "mantível" e organizada.</p>' +
    '<h2>Avaliação e Seleção</h2>' +
    '<p>Nosso objetivo aqui é transmitir e adquirir conhecimento e recrutar novos admins. Estamos atrás de dois perfis:</p>' +
    '<ul>' +
      '<li>A pessoa que vai documentar;</li>' +
      '<li>A pessoa que possui grande interesse étcnico e quer solucionar problemas</li>' +
    '</ul>' +
    '<p>Para isso, vamos observá-los e realizar anotações durante todo o curso. Façam contribuição no nosso site do curso; percebam quais problemas que a Rede atualmente possui tem a ver com alguma aula e como você resolveria isso; tire dúvida; faça sugestões; nos mande e-mail; combine de aparecer lá na admin; etc</p>' +
    '<h3>Exemplos de demonstração de interesse</h3>' +
    '<ol>' +
      '<li>' +
        '<p>O nosso NFS estava zoado quando novos usuários criaram suas contas em março, impedindo que eles conseguissem logar pela interfáce gráfica. Agora, para cada usuário que criou uma conta neste período, temos que rodar o comando abaixo. Sabendo que existe uma máquina com a home de todos os usuários, como você resolveria esse problema? Como chegou nessa solução?</p>' +
        '<code>chown -R &ltuser&gt:&ltcourse&gt /home/&ltcourse&gt/&ltuser&gt</code>' +
      '</li>' + 
      '<li>Como você faria para ter uma estimativa de quantos usuários temos ativos no momento? Qual seu critério de ativo?</li>' +
      '<li>Precisamos liberar espaço suspendendo o uso de quem já se formou. A USP não nos libera os dados dos alunos e não existe um atributo "data de criação" no nosso banco. Como você resolveria? O que faria para users futuros?</li>' +
      '<li>As máquinas estão muito lentas para o uso, principalmente durante o horário de almoço. Quais sua investigação sobre isso?</li>' +
    '</ol>' +
    '<p>Como dissemos, estamos procurando pessoas proativas e colaborativas. Percebam novos problemas, coisas que têm te irritado há tempos. Quais são suas sugestões? Nada precisa ser respondido agora. Pensando sobre durante a evolução do curso. Se você fosse admin, o que você faria?</p>' +
    '<p>Caso tenha se assustado com as perguntas, não se preocupe. Um primeiro passo para quem não tem conhecimento técnico é frequentar as aulas e tirar dúvidas. Isso também será observado. E também pode responder perguntas como:</p>' +
    '<ol>' +
      '<li>Como melhorar a integração e eficiência dos novos admins?</li>' +
      '<li>Como melhorar o trabalho em equipe?</li>' +
      '<li>Que mecanismos podemos usar para manter a sala mais limpa e organizada? </li>' +
    '</ol>' +
    '<p><strong>Perceba: Não há entregas e você não precisa responder nenhuma dessas perguntas. Elas são so sugestões de formas de mostrar envolvimento, que pode acontecer via e-mail, durante as aulas, telegram, etc. Participem como achar melhor!!</strong></p>' +
    '<h2>Palestrantes</h2>' +
    '<p>Durante todo o curso teremos diversos professores convidados especialistas em diversas áreas da computação. Como:</p>' +
    '<ul>' +
      '<li>Segurança;</li>' +
      '<li>Engenharia de Software</li>' +
      '<li>Sistemas</li>' +
      '<li>Teoria Computacional</li>' +
    '</ul>' +
    '<p>Hoje, os convidados são alguns ex-admins da Rede Linux, que vão contar como foi sua experiência e como isso influênciou sua entrada no Mercado de Trabalho. São eles:</p>' +
    '<h3>Patrícia Matsumoto</h3>' +
    '<p>Team Leader na Topaz, uma plataforma de soluções financeiras. É uma desenvolvedora analista de software relacionada a segurança de sistemas. Tem mais de 14 anos de carreira, e foi a primeira administradora, de 2003 até 2005.</p>' +
    '<h3>Cainã Setti</h3>' +
    '<p>Da 12a geração de admins - entre 2019 e 2021. Veterano do BCC, bolsista do Hacker do Bem, grande conhecedor de Hacker ético.</p>' +
    '<h3>Leonardo Rochael</h3>' +
    '<p>Principal Software Engineer na Luiza Labs, fundador da APyB - Associação Python Brasil, com tem como meta de apoiar as comunidades relacionadas à linguagem Python e derivadas tecnologias. Tem 21 anos de experiência em engenharia de software e da 1a geração de administradores.</p>' +
    '<h3>Prof. Daniel Cordeiro</h3>' +
    '<p>Foi admin entre 2001 e 2003, tem mais de 3 anos de experiência de mercado como engenheiro de software. Focou na carreira acadêmica, PhD em Matemática pela Grenoble University, na França e hoje é professor da EACH. Possui maior interesse em computação de alta performance.</p>'
  ];
